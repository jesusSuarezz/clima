import React, { Fragment, useState, useEffect } from 'react';
import Header from './components/Header';
import Form from './components/Form';
import Clima from './components/Clima';
import Error from './components/Error'
function App() {
	//State del formulario
	const [busqueda, saveBusqueda] = useState({
		ciudad: '',
		pais: '',
	});
	
	// g
	const [consultar, guardarConsultar] = useState(false);
	//guarda el resultado de la api
	const [resultado, guardarResultado] = useState({});
	//Guarda a respuesta del servidor 404,500
	const [error, guardarError] = useState(false);

	const { ciudad, pais } = busqueda;

	useEffect(() => {
		const consultarAPI = async () => {
			if (consultar) {
				const appId = 'd9f693b7ef0c04db7d5344146c7b31c9';
				const url = `https://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${appId}`;

				const respuesta = await fetch(url);
				const resultado = await respuesta.json();
				console.log(resultado);

				guardarResultado(resultado);
				guardarConsultar(false);
				if (resultado.cod === '404') {
					guardarError(true);
				} else {
					guardarError(false);
				}

				//detecta la respuesta del servidor segun la consulta
				if (resultado.cod === '404') {
					guardarError(true);
				} else {
					guardarError(false);
				}
			}
		};

		consultarAPI();

		//desactivar el error de las dependencias del useEffect
		// eslint-disable-next-line
	}, [consultar]);

	let componente;
	if (error) {
		componente = <Error mensaje="No hay resultados (Error 404)" />;
	} else {
		componente = <Clima resultado={resultado} />;
	}

	return (
		<Fragment>
			<Header titulo="Clima (Jesus Suarez)" />

			<div className="contenedor-form">
				<div className="row">
					<div className="col m6 s12">
						<Form
							busqueda={busqueda}
							saveBusqueda={saveBusqueda}
							guardarConsultar={guardarConsultar}
						/>
					</div>
					<div className="col m6 s12">{componente}</div>
				</div>
			</div>
		</Fragment>
	);
}

export default App;
