import React from 'react';
import PropTypes from 'prop-types';

const Clima = ({ resultado}) => {
	//estraer los valores de la respuesta de la api
	const { name, main } = resultado;
	//console.log(main);

	//de grados kelvin a celcius
	const kelvin = 273.15;

	if (!name) return null;
	return (
		<div class="card light-blue darken-2">
			<div class="card-content white-text">
				
				<span class="card-title">El clima de {name} es:</span>
				<p className="temperatura">
					{parseFloat(main.temp - kelvin, 10).toFixed(2)}
					<span>&#x2103;</span>
				</p>
			</div>
			<div class="card-action">
				<a href="#!">
					Temperatura maxima:
					{parseFloat(main.temp_max - kelvin, 10).toFixed(2)}
					<span>&#x2103;</span>
				</a>
				<a href="#!">
					Temperatura minima:
					{parseFloat(main.temp_min - kelvin, 10).toFixed(2)}
					<span>&#x2103;</span>
				</a>
			</div>
		</div>
	);
};

Clima.propTypes = {
	resultado: PropTypes.object.isRequired,
};

export default Clima;
