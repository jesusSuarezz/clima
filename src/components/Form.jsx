import React, { useState } from 'react';
import Error from './Error';
import PropTypes from 'prop-types';

const Form = ({ busqueda, saveBusqueda, guardarConsultar }) => {
	const [error, guardarError] = useState(false);

	//destructuring de ciudad y pais
	const { pais, ciudad } = busqueda;

	//funcion que coloca los elementos en el state
	const handleChange = (e) => {
		//actualizamos el state
		saveBusqueda({
			...busqueda,
			[e.target.name]: e.target.value,
		});
	};

	const handleSubmit = (e) => {
		//que no se ejecute el formulario al cargar por primera vez
		e.preventDefault();
		//validar
		if (pais.trim() === '' || ciudad.trim() === '') {
			guardarError(true);
			return;
		}
		guardarError(false);

		//pasarlo al componente principal
		guardarConsultar(true);
	};

	return (
		<form onSubmit={handleSubmit}>
			{error ? <Error mensaje="Todos los campos son obligatorios" /> : null}
			<div className="input-field col s12">
				<input
					type="text"
					name="ciudad"
					id="ciudad"
					value={ciudad}
					onChange={handleChange}
				/>
				<label htmlFor="ciudad">Ciudad</label>
			</div>
			<div className="input-field col s12">
				<select name="pais" id="pais" value={pais} onChange={handleChange}>
					<option value="US">-- Selecciona un país</option>
					<option value="US">Estados Unidos</option>
					<option value="MX">México</option>
					<option value="AR">Argentina</option>
					<option value="CO">Colombia</option>
					<option value="CR">Costa Rica</option>
					<option value="ES">España</option>
					<option value="PE">Perú</option>
				</select>
				<label htmlFor="pais">País: </label>
			</div>
			<div className="input-field col s12">
				<button
					className="btn waves-effect waves-light"
					type="submit"
				>
					Buscar clima
				</button>
			</div>
		</form>
	);
};

Form.propTypes = {
	busqueda: PropTypes.object.isRequired,
	saveBusqueda: PropTypes.func.isRequired,
	guardarConsultar: PropTypes.func.isRequired,
};

export default Form;
